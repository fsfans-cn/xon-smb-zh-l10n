# xon-smb-zh-l10n

(Unofficial) Chinese translation for [Xonotic](https://xonotic.org/) + [SMB mod pack](https://github.com/MarioSMB/modpack)

## Infrastructure

- Using `1-update.sh`,
    - `common.pot` from [upstream Xonotic data repository](https://gitlab.com/xonotic/xonotic-data.pk3dir) is regularly updated
    - `common.zh_Hant.po` is firstly fetched from [my another repository](https://github.com/NaitLee/xon-zh-grand-l10n), then being updated incrementally in-place
    - `csprogs.dat.pot`, `menu.dat.pot` and `progs.dat.pot` are generated on my machine:
        - compile SMB mod pack and use it locally
        - set the cvar `prvm_language` to `"dump"`, start a local server, so that strings from both vanilla Xonotic and SMB code are dumped as these files
    - All of these files are concatenated with GNU gettext `msgcat`, updating `common.zh_Hant.po`
    - `common.zh_Hant.po` is edited directly
- Using `0-convert.sh` with `opencc`, conversion to other Chinese variants are done

## Usage

- Clone this repository, put to Xonotic data directory using whichever method, enjoy locally;
- or [package](https://src.fsfans.club/naitlee/xonsrvcfg/src/branch/main/mkpk3.py) to a `.pk3` archive, put to your server.
- If you want a few server-side strings translated, don’t forget to set `prvm_language "zh_*"` in your server config file.
    - note: this isn’t officially implemented yet.

## Notes

- All of these work are unofficial, neither (technically) supported by Xonotic nor SMB mod pack authors.
- A handful of strings are not translated yet, for I have not seen yet.
- The translation files contain some “private” strings as for now, for I’m just [patching](https://github.com/NaitLee/xonotic-smb-modpack/tree/NaitLee/more-localizable-strings) the modpack for better localization support.
- Wondering why there are server-side strings inside? (like radio announcement string) Read the previous note, and please understand that, a server *may* *intend to* serve *only* players of its own locale, either for physical limitation (very high latency oversea) or for something else. Besides that, if done properly, everyone can understand what’s going on just by glancing at a string when it appears, without understanding it grammatically.
