#!/bin/sh
xonotic_data="/mnt/data/@/git-repo/xonotic/data/xonotic-data.pk3dir"
userdata="$HOME/.xonotic/data"
generated="$userdata/csprogs.dat.pot $userdata/menu.dat.pot $userdata/progs.dat.pot"
if ! {
    cp common.zh_Hant.po common.zh_Hant.po.old
    for po in $generated; do {
        if [[ x`sed 1q < $po` != x'msgid ""' ]]; then {
            mv $po $po.bad
            cat >$po <<EOF
msgid ""
msgstr ""
"Content-Transfer-Encoding: 8bit\n"
"Content-Type: text/plain; charset=UTF-8\n"
EOF
            cat $po.bad >>$po
            rm $po.bad
        }; fi
    }; done
    msgcat common.zh_Hant.po "$xonotic_data/common.pot" $generated | python3 postprocess.py >>new.po
    mv new.po common.zh_Hant.po
}; then ./2-revert.sh; fi
